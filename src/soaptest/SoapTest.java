/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package soaptest;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.huawei.evcinterface.portalmgrmsg.PrepaidRechargeRequestMsg;
import com.huawei.evcinterface.common.RequestHeader;
import com.huawei.evcinterface.common.SessionEntityType;
import com.huawei.evcinterface.portalmgr.EVCInterfacePortalMgrServiceCustomization;
import com.huawei.evcinterface.portalmgr.EVCInterfacePortalMgrServiceCustomizationPortType;
import com.huawei.evcinterface.portalmgr.PrepaidRechargeRequest;
import com.huawei.evcinterface.portalmgrmsg.PrepaidRechargeResultMsg;

/**
 *
 * @author FARzad
 */
public class SoapTest {

    /**
     * @param args the command line arguments
     */


    public static void main(String[] args) {
        SoapTestCommand soapTest = new SoapTestCommand();
        new JCommander(soapTest, args);

        String result = sendRequestHeader(soapTest.getRSMSISDN(),soapTest.getMSISDN(),soapTest.getValue());
        System.out.println("Result is : "+result);

    }

    private static class SoapTestCommand {

        @Parameter(names = "-value", required = true, description = "101 for 100mb reward, 102 for 25mb reward")
        protected String value;

        @Parameter(names = "-rsmsisdn", required = true, description = "RSMSISDN, eg 89689399000")
        protected String rsmsisdn;

        @Parameter(names = "--msisdn", required = true, description = "Phone MSISDN, eg 8979701000")
        protected String msisdn;

        public String getValue() {
            return value;

        }

        public String getRSMSISDN() {
            return rsmsisdn;
        }

        public String getMSISDN() {
            return msisdn;
        }

    }

    private static PrepaidRechargeRequest getPrepaidRechargeRequest(String RSMSISDN, String MSISDN, String clientId, String value,
            String pin, String rechargeType) {

        PrepaidRechargeRequest prepaidRechargeRequest = new PrepaidRechargeRequest();
        prepaidRechargeRequest.setRSMSISDN(RSMSISDN);
        prepaidRechargeRequest.setMSISDN(MSISDN);
        prepaidRechargeRequest.setCLIENTID(clientId);
        prepaidRechargeRequest.setVALUE(value);
        prepaidRechargeRequest.setPIN(pin);
        prepaidRechargeRequest.setRECHARGETYPE(rechargeType);

        return prepaidRechargeRequest;
    }

    private static SessionEntityType getSessionEntityType(String Name, String Password, String RemoteAddress) {

        SessionEntityType sessionEntityType = new SessionEntityType();
        sessionEntityType.setName(Name);
        sessionEntityType.setPassword(Password);
        sessionEntityType.setRemoteAddress(RemoteAddress);

        return sessionEntityType;
    }

    private static RequestHeader getRequestHeader(String CommandId, String Version, String AdditionInfo, SessionEntityType sessionEntityType) {

        RequestHeader requestHeader = new RequestHeader();
        requestHeader.setCommandId(CommandId);
        requestHeader.setVersion(Version);
//        requestHeader.setAdditionInfo(AdditionInfo);
        requestHeader.setSessionEntity(sessionEntityType);

        return requestHeader;
    }

    private static String sendRequestHeader(String RSMSISDN, String MSISDN, String value) {

        EVCInterfacePortalMgrServiceCustomization evcIPMSC = new EVCInterfacePortalMgrServiceCustomization();
        EVCInterfacePortalMgrServiceCustomizationPortType evcIPMSCPort = evcIPMSC.getEVCInterfacePortalMgrServiceCustomizationSOAP11PortHttp();
        PrepaidRechargeRequestMsg msg = new PrepaidRechargeRequestMsg();
        PrepaidRechargeRequest prepaidRechargeRequest = getPrepaidRechargeRequest(RSMSISDN, MSISDN, "0", value, "321432", "1");
//        PrepaidRechargeRequest prepaidRechargeRequest = getPrepaidRechargeRequest("89689399054", "8979701277", "0", "102", "321432", "1");
        msg.setPrepaidRechargeRequest(prepaidRechargeRequest);
        SessionEntityType sessionEntityType = getSessionEntityType("evwserv", "TELLIN", "?");
        RequestHeader requestHeader = getRequestHeader("PrepaidRecharge", "1", "?", sessionEntityType);

        msg.setRequestHeader(requestHeader);
        PrepaidRechargeResultMsg result = evcIPMSCPort.prepaidRecharge(msg);

        System.out.println("Result Code : " + result.getResultHeader().getResultCode());
        System.out.println("Result CommandID: " + result.getResultHeader().getCommandId());
        System.out.println("Result Description: " + result.getResultHeader().getResultDesc());
        System.out.println("Result SerialNo : " + result.getResultHeader().getSerialNo());
        
        return result.getResultHeader().getResultCode();
    }

}
