
package com.huawei.evcinterface.portalmgrmsg;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.evcinterface.common.RequestHeader;
import com.huawei.evcinterface.portalmgr.QueryChildInfoRequest;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RequestHeader" type="{http://www.huawei.com/evcinterface/common}RequestHeader"/>
 *         &lt;element name="QueryChildInfoRequest" type="{http://www.huawei.com/evcinterface/portalmgr}QueryChildInfoRequest"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "requestHeader",
    "queryChildInfoRequest"
})
@XmlRootElement(name = "QueryChildInfoRequestMsg")
public class QueryChildInfoRequestMsg {

    @XmlElement(name = "RequestHeader", required = true)
    protected RequestHeader requestHeader;
    @XmlElement(name = "QueryChildInfoRequest", required = true)
    protected QueryChildInfoRequest queryChildInfoRequest;

    /**
     * Gets the value of the requestHeader property.
     * 
     * @return
     *     possible object is
     *     {@link RequestHeader }
     *     
     */
    public RequestHeader getRequestHeader() {
        return requestHeader;
    }

    /**
     * Sets the value of the requestHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestHeader }
     *     
     */
    public void setRequestHeader(RequestHeader value) {
        this.requestHeader = value;
    }

    /**
     * Gets the value of the queryChildInfoRequest property.
     * 
     * @return
     *     possible object is
     *     {@link QueryChildInfoRequest }
     *     
     */
    public QueryChildInfoRequest getQueryChildInfoRequest() {
        return queryChildInfoRequest;
    }

    /**
     * Sets the value of the queryChildInfoRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryChildInfoRequest }
     *     
     */
    public void setQueryChildInfoRequest(QueryChildInfoRequest value) {
        this.queryChildInfoRequest = value;
    }

}
