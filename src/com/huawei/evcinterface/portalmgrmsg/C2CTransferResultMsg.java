
package com.huawei.evcinterface.portalmgrmsg;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.evcinterface.common.ResultHeader;
import com.huawei.evcinterface.portalmgr.C2CTransferResult;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ResultHeader" type="{http://www.huawei.com/evcinterface/common}ResultHeader"/>
 *         &lt;element name="C2CTransferResult" type="{http://www.huawei.com/evcinterface/portalmgr}C2CTransferResult"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "resultHeader",
    "c2CTransferResult"
})
@XmlRootElement(name = "C2CTransferResultMsg")
public class C2CTransferResultMsg {

    @XmlElement(name = "ResultHeader", required = true)
    protected ResultHeader resultHeader;
    @XmlElement(name = "C2CTransferResult", required = true)
    protected C2CTransferResult c2CTransferResult;

    /**
     * Gets the value of the resultHeader property.
     * 
     * @return
     *     possible object is
     *     {@link ResultHeader }
     *     
     */
    public ResultHeader getResultHeader() {
        return resultHeader;
    }

    /**
     * Sets the value of the resultHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResultHeader }
     *     
     */
    public void setResultHeader(ResultHeader value) {
        this.resultHeader = value;
    }

    /**
     * Gets the value of the c2CTransferResult property.
     * 
     * @return
     *     possible object is
     *     {@link C2CTransferResult }
     *     
     */
    public C2CTransferResult getC2CTransferResult() {
        return c2CTransferResult;
    }

    /**
     * Sets the value of the c2CTransferResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link C2CTransferResult }
     *     
     */
    public void setC2CTransferResult(C2CTransferResult value) {
        this.c2CTransferResult = value;
    }

}
