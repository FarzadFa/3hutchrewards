
package com.huawei.evcinterface.portalmgrmsg;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.evcinterface.common.ResultHeader;
import com.huawei.evcinterface.portalmgr.QueryInventoryResult;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ResultHeader" type="{http://www.huawei.com/evcinterface/common}ResultHeader"/>
 *         &lt;element name="QueryInventoryResult" type="{http://www.huawei.com/evcinterface/portalmgr}QueryInventoryResult"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "resultHeader",
    "queryInventoryResult"
})
@XmlRootElement(name = "QueryInventoryResultMsg")
public class QueryInventoryResultMsg {

    @XmlElement(name = "ResultHeader", required = true)
    protected ResultHeader resultHeader;
    @XmlElement(name = "QueryInventoryResult", required = true)
    protected QueryInventoryResult queryInventoryResult;

    /**
     * Gets the value of the resultHeader property.
     * 
     * @return
     *     possible object is
     *     {@link ResultHeader }
     *     
     */
    public ResultHeader getResultHeader() {
        return resultHeader;
    }

    /**
     * Sets the value of the resultHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResultHeader }
     *     
     */
    public void setResultHeader(ResultHeader value) {
        this.resultHeader = value;
    }

    /**
     * Gets the value of the queryInventoryResult property.
     * 
     * @return
     *     possible object is
     *     {@link QueryInventoryResult }
     *     
     */
    public QueryInventoryResult getQueryInventoryResult() {
        return queryInventoryResult;
    }

    /**
     * Sets the value of the queryInventoryResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryInventoryResult }
     *     
     */
    public void setQueryInventoryResult(QueryInventoryResult value) {
        this.queryInventoryResult = value;
    }

}
