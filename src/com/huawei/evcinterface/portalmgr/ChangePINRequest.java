
package com.huawei.evcinterface.portalmgr;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ChangePINRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ChangePINRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="HPNO" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="OldPIN" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="NewPIN" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CFMPIN" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ChangePINRequest", propOrder = {
    "hpno",
    "oldPIN",
    "newPIN",
    "cfmpin"
})
public class ChangePINRequest {

    @XmlElement(name = "HPNO", required = true)
    protected String hpno;
    @XmlElement(name = "OldPIN", required = true)
    protected String oldPIN;
    @XmlElement(name = "NewPIN", required = true)
    protected String newPIN;
    @XmlElement(name = "CFMPIN", required = true)
    protected String cfmpin;

    /**
     * Gets the value of the hpno property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHPNO() {
        return hpno;
    }

    /**
     * Sets the value of the hpno property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHPNO(String value) {
        this.hpno = value;
    }

    /**
     * Gets the value of the oldPIN property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOldPIN() {
        return oldPIN;
    }

    /**
     * Sets the value of the oldPIN property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOldPIN(String value) {
        this.oldPIN = value;
    }

    /**
     * Gets the value of the newPIN property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNewPIN() {
        return newPIN;
    }

    /**
     * Sets the value of the newPIN property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNewPIN(String value) {
        this.newPIN = value;
    }

    /**
     * Gets the value of the cfmpin property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCFMPIN() {
        return cfmpin;
    }

    /**
     * Sets the value of the cfmpin property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCFMPIN(String value) {
        this.cfmpin = value;
    }

}
