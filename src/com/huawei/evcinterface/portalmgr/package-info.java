/**
 * EVCInterfacePortalMgrService_Customization
 * 
 */
@javax.xml.bind.annotation.XmlSchema(namespace = "http://www.huawei.com/evcinterface/portalmgr", elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED)
package com.huawei.evcinterface.portalmgr;
