
package com.huawei.evcinterface.portalmgr;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ModifyLanguageRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ModifyLanguageRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="HPNO" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Langtype" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ModifyLanguageRequest", propOrder = {
    "hpno",
    "langtype"
})
public class ModifyLanguageRequest {

    @XmlElement(name = "HPNO", required = true)
    protected String hpno;
    @XmlElement(name = "Langtype", required = true)
    protected String langtype;

    /**
     * Gets the value of the hpno property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHPNO() {
        return hpno;
    }

    /**
     * Sets the value of the hpno property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHPNO(String value) {
        this.hpno = value;
    }

    /**
     * Gets the value of the langtype property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLangtype() {
        return langtype;
    }

    /**
     * Sets the value of the langtype property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLangtype(String value) {
        this.langtype = value;
    }

}
