
package com.huawei.evcinterface.portalmgr;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Logger;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceFeature;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.1.6
 * Generated source version: 2.1
 * 
 */
@WebServiceClient(name = "EVCInterfacePortalMgrService_Customization", targetNamespace = "http://www.huawei.com/evcinterface/portalmgr", wsdlLocation = "http://10.70.11.70:8080/services/EVCInterfacePortalMgrService_Customization?wsdl")
public class EVCInterfacePortalMgrServiceCustomization
    extends Service
{

    private final static URL EVCINTERFACEPORTALMGRSERVICECUSTOMIZATION_WSDL_LOCATION;
    private final static Logger logger = Logger.getLogger(com.huawei.evcinterface.portalmgr.EVCInterfacePortalMgrServiceCustomization.class.getName());

    static {
        URL url = null;
        try {
            URL baseUrl;
            baseUrl = com.huawei.evcinterface.portalmgr.EVCInterfacePortalMgrServiceCustomization.class.getResource(".");
            url = new URL(baseUrl, "http://10.70.11.70:8080/services/EVCInterfacePortalMgrService_Customization?wsdl");
        } catch (MalformedURLException e) {
            logger.warning("Failed to create URL for the wsdl Location: 'http://10.70.11.70:8080/services/EVCInterfacePortalMgrService_Customization?wsdl', retrying as a local file");
            logger.warning(e.getMessage());
        }
        EVCINTERFACEPORTALMGRSERVICECUSTOMIZATION_WSDL_LOCATION = url;
    }

    public EVCInterfacePortalMgrServiceCustomization(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public EVCInterfacePortalMgrServiceCustomization() {
        super(EVCINTERFACEPORTALMGRSERVICECUSTOMIZATION_WSDL_LOCATION, new QName("http://www.huawei.com/evcinterface/portalmgr", "EVCInterfacePortalMgrService_Customization"));
    }

    /**
     * 
     * @return
     *     returns EVCInterfacePortalMgrServiceCustomizationPortType
     */
    @WebEndpoint(name = "EVCInterfacePortalMgrService_CustomizationSOAP11port_http")
    public EVCInterfacePortalMgrServiceCustomizationPortType getEVCInterfacePortalMgrServiceCustomizationSOAP11PortHttp() {
        return super.getPort(new QName("http://www.huawei.com/evcinterface/portalmgr", "EVCInterfacePortalMgrService_CustomizationSOAP11port_http"), EVCInterfacePortalMgrServiceCustomizationPortType.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns EVCInterfacePortalMgrServiceCustomizationPortType
     */
    @WebEndpoint(name = "EVCInterfacePortalMgrService_CustomizationSOAP11port_http")
    public EVCInterfacePortalMgrServiceCustomizationPortType getEVCInterfacePortalMgrServiceCustomizationSOAP11PortHttp(WebServiceFeature... features) {
        return super.getPort(new QName("http://www.huawei.com/evcinterface/portalmgr", "EVCInterfacePortalMgrService_CustomizationSOAP11port_http"), EVCInterfacePortalMgrServiceCustomizationPortType.class, features);
    }

    /**
     * 
     * @return
     *     returns EVCInterfacePortalMgrServiceCustomizationPortType
     */
    @WebEndpoint(name = "EVCInterfacePortalMgrService_CustomizationSOAP12port_http")
    public EVCInterfacePortalMgrServiceCustomizationPortType getEVCInterfacePortalMgrServiceCustomizationSOAP12PortHttp() {
        return super.getPort(new QName("http://www.huawei.com/evcinterface/portalmgr", "EVCInterfacePortalMgrService_CustomizationSOAP12port_http"), EVCInterfacePortalMgrServiceCustomizationPortType.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns EVCInterfacePortalMgrServiceCustomizationPortType
     */
    @WebEndpoint(name = "EVCInterfacePortalMgrService_CustomizationSOAP12port_http")
    public EVCInterfacePortalMgrServiceCustomizationPortType getEVCInterfacePortalMgrServiceCustomizationSOAP12PortHttp(WebServiceFeature... features) {
        return super.getPort(new QName("http://www.huawei.com/evcinterface/portalmgr", "EVCInterfacePortalMgrService_CustomizationSOAP12port_http"), EVCInterfacePortalMgrServiceCustomizationPortType.class, features);
    }

    /**
     * 
     * @return
     *     returns EVCInterfacePortalMgrServiceCustomizationPortType
     */
    @WebEndpoint(name = "EVCInterfacePortalMgrService_CustomizationHttpport1")
    public EVCInterfacePortalMgrServiceCustomizationPortType getEVCInterfacePortalMgrServiceCustomizationHttpport1() {
        return super.getPort(new QName("http://www.huawei.com/evcinterface/portalmgr", "EVCInterfacePortalMgrService_CustomizationHttpport1"), EVCInterfacePortalMgrServiceCustomizationPortType.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns EVCInterfacePortalMgrServiceCustomizationPortType
     */
    @WebEndpoint(name = "EVCInterfacePortalMgrService_CustomizationHttpport1")
    public EVCInterfacePortalMgrServiceCustomizationPortType getEVCInterfacePortalMgrServiceCustomizationHttpport1(WebServiceFeature... features) {
        return super.getPort(new QName("http://www.huawei.com/evcinterface/portalmgr", "EVCInterfacePortalMgrService_CustomizationHttpport1"), EVCInterfacePortalMgrServiceCustomizationPortType.class, features);
    }

}
