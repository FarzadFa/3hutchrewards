
package com.huawei.evcinterface.portalmgr;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for QueryTransactionResult complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="QueryTransactionResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TransactionResult" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="TransactionID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="TransactionTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="OperationType" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="HPNO" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="TOHPNO" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="ACCINDEX" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="PriceType" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                   &lt;element name="Quantity" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="SRCValue" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                   &lt;element name="DESTValue" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                   &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QueryTransactionResult", propOrder = {
    "transactionResult"
})
public class QueryTransactionResult {

    @XmlElement(name = "TransactionResult")
    protected List<QueryTransactionResult.TransactionResult> transactionResult;

    /**
     * Gets the value of the transactionResult property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the transactionResult property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTransactionResult().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link QueryTransactionResult.TransactionResult }
     * 
     * 
     */
    public List<QueryTransactionResult.TransactionResult> getTransactionResult() {
        if (transactionResult == null) {
            transactionResult = new ArrayList<QueryTransactionResult.TransactionResult>();
        }
        return this.transactionResult;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="TransactionID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="TransactionTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="OperationType" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="HPNO" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="TOHPNO" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="ACCINDEX" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="PriceType" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *         &lt;element name="Quantity" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="SRCValue" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *         &lt;element name="DESTValue" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *         &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "transactionID",
        "transactionTime",
        "operationType",
        "hpno",
        "tohpno",
        "accindex",
        "priceType",
        "quantity",
        "srcValue",
        "destValue",
        "status"
    })
    public static class TransactionResult {

        @XmlElement(name = "TransactionID", required = true)
        protected String transactionID;
        @XmlElement(name = "TransactionTime", required = true)
        protected String transactionTime;
        @XmlElement(name = "OperationType")
        protected int operationType;
        @XmlElement(name = "HPNO", required = true)
        protected String hpno;
        @XmlElement(name = "TOHPNO", required = true)
        protected String tohpno;
        @XmlElement(name = "ACCINDEX")
        protected int accindex;
        @XmlElement(name = "PriceType")
        protected long priceType;
        @XmlElement(name = "Quantity")
        protected int quantity;
        @XmlElement(name = "SRCValue")
        protected long srcValue;
        @XmlElement(name = "DESTValue")
        protected long destValue;
        @XmlElement(name = "Status")
        protected int status;

        /**
         * Gets the value of the transactionID property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTransactionID() {
            return transactionID;
        }

        /**
         * Sets the value of the transactionID property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTransactionID(String value) {
            this.transactionID = value;
        }

        /**
         * Gets the value of the transactionTime property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTransactionTime() {
            return transactionTime;
        }

        /**
         * Sets the value of the transactionTime property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTransactionTime(String value) {
            this.transactionTime = value;
        }

        /**
         * Gets the value of the operationType property.
         * 
         */
        public int getOperationType() {
            return operationType;
        }

        /**
         * Sets the value of the operationType property.
         * 
         */
        public void setOperationType(int value) {
            this.operationType = value;
        }

        /**
         * Gets the value of the hpno property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getHPNO() {
            return hpno;
        }

        /**
         * Sets the value of the hpno property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setHPNO(String value) {
            this.hpno = value;
        }

        /**
         * Gets the value of the tohpno property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTOHPNO() {
            return tohpno;
        }

        /**
         * Sets the value of the tohpno property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTOHPNO(String value) {
            this.tohpno = value;
        }

        /**
         * Gets the value of the accindex property.
         * 
         */
        public int getACCINDEX() {
            return accindex;
        }

        /**
         * Sets the value of the accindex property.
         * 
         */
        public void setACCINDEX(int value) {
            this.accindex = value;
        }

        /**
         * Gets the value of the priceType property.
         * 
         */
        public long getPriceType() {
            return priceType;
        }

        /**
         * Sets the value of the priceType property.
         * 
         */
        public void setPriceType(long value) {
            this.priceType = value;
        }

        /**
         * Gets the value of the quantity property.
         * 
         */
        public int getQuantity() {
            return quantity;
        }

        /**
         * Sets the value of the quantity property.
         * 
         */
        public void setQuantity(int value) {
            this.quantity = value;
        }

        /**
         * Gets the value of the srcValue property.
         * 
         */
        public long getSRCValue() {
            return srcValue;
        }

        /**
         * Sets the value of the srcValue property.
         * 
         */
        public void setSRCValue(long value) {
            this.srcValue = value;
        }

        /**
         * Gets the value of the destValue property.
         * 
         */
        public long getDESTValue() {
            return destValue;
        }

        /**
         * Sets the value of the destValue property.
         * 
         */
        public void setDESTValue(long value) {
            this.destValue = value;
        }

        /**
         * Gets the value of the status property.
         * 
         */
        public int getStatus() {
            return status;
        }

        /**
         * Sets the value of the status property.
         * 
         */
        public void setStatus(int value) {
            this.status = value;
        }

    }

}
