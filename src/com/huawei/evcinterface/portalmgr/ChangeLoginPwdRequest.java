
package com.huawei.evcinterface.portalmgr;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ChangeLoginPwdRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ChangeLoginPwdRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="HPNO" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="OldPWD" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="NewPWD" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CFMPwd" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ChangeLoginPwdRequest", propOrder = {
    "hpno",
    "oldPWD",
    "newPWD",
    "cfmPwd"
})
public class ChangeLoginPwdRequest {

    @XmlElement(name = "HPNO", required = true)
    protected String hpno;
    @XmlElement(name = "OldPWD", required = true)
    protected String oldPWD;
    @XmlElement(name = "NewPWD", required = true)
    protected String newPWD;
    @XmlElement(name = "CFMPwd", required = true)
    protected String cfmPwd;

    /**
     * Gets the value of the hpno property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHPNO() {
        return hpno;
    }

    /**
     * Sets the value of the hpno property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHPNO(String value) {
        this.hpno = value;
    }

    /**
     * Gets the value of the oldPWD property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOldPWD() {
        return oldPWD;
    }

    /**
     * Sets the value of the oldPWD property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOldPWD(String value) {
        this.oldPWD = value;
    }

    /**
     * Gets the value of the newPWD property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNewPWD() {
        return newPWD;
    }

    /**
     * Sets the value of the newPWD property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNewPWD(String value) {
        this.newPWD = value;
    }

    /**
     * Gets the value of the cfmPwd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCFMPwd() {
        return cfmPwd;
    }

    /**
     * Sets the value of the cfmPwd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCFMPwd(String value) {
        this.cfmPwd = value;
    }

}
