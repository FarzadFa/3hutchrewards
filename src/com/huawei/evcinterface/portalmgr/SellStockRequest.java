
package com.huawei.evcinterface.portalmgr;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SellStockRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SellStockRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="HPNO" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Price" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Amout" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Price1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Amout1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Price2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Amout2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Price3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Amout3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Price4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Amout4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Price5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Amout5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Price6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Amout6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Price7" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Amout7" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Price8" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Amout8" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Price9" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Amout9" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Price10" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Amout10" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Price11" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Amout11" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Price12" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Amout12" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Price13" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Amout13" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Price14" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Amout14" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Price15" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Amout15" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Price16" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Amout16" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Price17" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Amout17" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Price18" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Amout18" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Price19" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Amout19" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Price20" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Amout20" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Price21" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Amout21" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Price22" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Amout22" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Price23" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Amout23" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Price24" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Amout24" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Price25" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Amout25" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Price26" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Amout26" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Price27" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Amout27" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Price28" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Amout28" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Price29" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Amout29" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Reason" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SellStockRequest", propOrder = {
    "hpno",
    "price",
    "amout",
    "price1",
    "amout1",
    "price2",
    "amout2",
    "price3",
    "amout3",
    "price4",
    "amout4",
    "price5",
    "amout5",
    "price6",
    "amout6",
    "price7",
    "amout7",
    "price8",
    "amout8",
    "price9",
    "amout9",
    "price10",
    "amout10",
    "price11",
    "amout11",
    "price12",
    "amout12",
    "price13",
    "amout13",
    "price14",
    "amout14",
    "price15",
    "amout15",
    "price16",
    "amout16",
    "price17",
    "amout17",
    "price18",
    "amout18",
    "price19",
    "amout19",
    "price20",
    "amout20",
    "price21",
    "amout21",
    "price22",
    "amout22",
    "price23",
    "amout23",
    "price24",
    "amout24",
    "price25",
    "amout25",
    "price26",
    "amout26",
    "price27",
    "amout27",
    "price28",
    "amout28",
    "price29",
    "amout29",
    "reason"
})
public class SellStockRequest {

    @XmlElement(name = "HPNO", required = true)
    protected String hpno;
    @XmlElement(name = "Price")
    protected String price;
    @XmlElement(name = "Amout", required = true)
    protected String amout;
    @XmlElement(name = "Price1")
    protected String price1;
    @XmlElement(name = "Amout1")
    protected String amout1;
    @XmlElement(name = "Price2")
    protected String price2;
    @XmlElement(name = "Amout2")
    protected String amout2;
    @XmlElement(name = "Price3")
    protected String price3;
    @XmlElement(name = "Amout3")
    protected String amout3;
    @XmlElement(name = "Price4")
    protected String price4;
    @XmlElement(name = "Amout4")
    protected String amout4;
    @XmlElement(name = "Price5")
    protected String price5;
    @XmlElement(name = "Amout5")
    protected String amout5;
    @XmlElement(name = "Price6")
    protected String price6;
    @XmlElement(name = "Amout6")
    protected String amout6;
    @XmlElement(name = "Price7")
    protected String price7;
    @XmlElement(name = "Amout7")
    protected String amout7;
    @XmlElement(name = "Price8")
    protected String price8;
    @XmlElement(name = "Amout8")
    protected String amout8;
    @XmlElement(name = "Price9")
    protected String price9;
    @XmlElement(name = "Amout9")
    protected String amout9;
    @XmlElement(name = "Price10")
    protected String price10;
    @XmlElement(name = "Amout10")
    protected String amout10;
    @XmlElement(name = "Price11")
    protected String price11;
    @XmlElement(name = "Amout11")
    protected String amout11;
    @XmlElement(name = "Price12")
    protected String price12;
    @XmlElement(name = "Amout12")
    protected String amout12;
    @XmlElement(name = "Price13")
    protected String price13;
    @XmlElement(name = "Amout13")
    protected String amout13;
    @XmlElement(name = "Price14")
    protected String price14;
    @XmlElement(name = "Amout14")
    protected String amout14;
    @XmlElement(name = "Price15")
    protected String price15;
    @XmlElement(name = "Amout15")
    protected String amout15;
    @XmlElement(name = "Price16")
    protected String price16;
    @XmlElement(name = "Amout16")
    protected String amout16;
    @XmlElement(name = "Price17")
    protected String price17;
    @XmlElement(name = "Amout17")
    protected String amout17;
    @XmlElement(name = "Price18")
    protected String price18;
    @XmlElement(name = "Amout18")
    protected String amout18;
    @XmlElement(name = "Price19")
    protected String price19;
    @XmlElement(name = "Amout19")
    protected String amout19;
    @XmlElement(name = "Price20")
    protected String price20;
    @XmlElement(name = "Amout20")
    protected String amout20;
    @XmlElement(name = "Price21")
    protected String price21;
    @XmlElement(name = "Amout21")
    protected String amout21;
    @XmlElement(name = "Price22")
    protected String price22;
    @XmlElement(name = "Amout22")
    protected String amout22;
    @XmlElement(name = "Price23")
    protected String price23;
    @XmlElement(name = "Amout23")
    protected String amout23;
    @XmlElement(name = "Price24")
    protected String price24;
    @XmlElement(name = "Amout24")
    protected String amout24;
    @XmlElement(name = "Price25")
    protected String price25;
    @XmlElement(name = "Amout25")
    protected String amout25;
    @XmlElement(name = "Price26")
    protected String price26;
    @XmlElement(name = "Amout26")
    protected String amout26;
    @XmlElement(name = "Price27")
    protected String price27;
    @XmlElement(name = "Amout27")
    protected String amout27;
    @XmlElement(name = "Price28")
    protected String price28;
    @XmlElement(name = "Amout28")
    protected String amout28;
    @XmlElement(name = "Price29")
    protected String price29;
    @XmlElement(name = "Amout29")
    protected String amout29;
    @XmlElement(name = "Reason", required = true)
    protected String reason;

    /**
     * Gets the value of the hpno property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHPNO() {
        return hpno;
    }

    /**
     * Sets the value of the hpno property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHPNO(String value) {
        this.hpno = value;
    }

    /**
     * Gets the value of the price property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrice() {
        return price;
    }

    /**
     * Sets the value of the price property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrice(String value) {
        this.price = value;
    }

    /**
     * Gets the value of the amout property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAmout() {
        return amout;
    }

    /**
     * Sets the value of the amout property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAmout(String value) {
        this.amout = value;
    }

    /**
     * Gets the value of the price1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrice1() {
        return price1;
    }

    /**
     * Sets the value of the price1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrice1(String value) {
        this.price1 = value;
    }

    /**
     * Gets the value of the amout1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAmout1() {
        return amout1;
    }

    /**
     * Sets the value of the amout1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAmout1(String value) {
        this.amout1 = value;
    }

    /**
     * Gets the value of the price2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrice2() {
        return price2;
    }

    /**
     * Sets the value of the price2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrice2(String value) {
        this.price2 = value;
    }

    /**
     * Gets the value of the amout2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAmout2() {
        return amout2;
    }

    /**
     * Sets the value of the amout2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAmout2(String value) {
        this.amout2 = value;
    }

    /**
     * Gets the value of the price3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrice3() {
        return price3;
    }

    /**
     * Sets the value of the price3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrice3(String value) {
        this.price3 = value;
    }

    /**
     * Gets the value of the amout3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAmout3() {
        return amout3;
    }

    /**
     * Sets the value of the amout3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAmout3(String value) {
        this.amout3 = value;
    }

    /**
     * Gets the value of the price4 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrice4() {
        return price4;
    }

    /**
     * Sets the value of the price4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrice4(String value) {
        this.price4 = value;
    }

    /**
     * Gets the value of the amout4 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAmout4() {
        return amout4;
    }

    /**
     * Sets the value of the amout4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAmout4(String value) {
        this.amout4 = value;
    }

    /**
     * Gets the value of the price5 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrice5() {
        return price5;
    }

    /**
     * Sets the value of the price5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrice5(String value) {
        this.price5 = value;
    }

    /**
     * Gets the value of the amout5 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAmout5() {
        return amout5;
    }

    /**
     * Sets the value of the amout5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAmout5(String value) {
        this.amout5 = value;
    }

    /**
     * Gets the value of the price6 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrice6() {
        return price6;
    }

    /**
     * Sets the value of the price6 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrice6(String value) {
        this.price6 = value;
    }

    /**
     * Gets the value of the amout6 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAmout6() {
        return amout6;
    }

    /**
     * Sets the value of the amout6 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAmout6(String value) {
        this.amout6 = value;
    }

    /**
     * Gets the value of the price7 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrice7() {
        return price7;
    }

    /**
     * Sets the value of the price7 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrice7(String value) {
        this.price7 = value;
    }

    /**
     * Gets the value of the amout7 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAmout7() {
        return amout7;
    }

    /**
     * Sets the value of the amout7 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAmout7(String value) {
        this.amout7 = value;
    }

    /**
     * Gets the value of the price8 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrice8() {
        return price8;
    }

    /**
     * Sets the value of the price8 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrice8(String value) {
        this.price8 = value;
    }

    /**
     * Gets the value of the amout8 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAmout8() {
        return amout8;
    }

    /**
     * Sets the value of the amout8 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAmout8(String value) {
        this.amout8 = value;
    }

    /**
     * Gets the value of the price9 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrice9() {
        return price9;
    }

    /**
     * Sets the value of the price9 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrice9(String value) {
        this.price9 = value;
    }

    /**
     * Gets the value of the amout9 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAmout9() {
        return amout9;
    }

    /**
     * Sets the value of the amout9 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAmout9(String value) {
        this.amout9 = value;
    }

    /**
     * Gets the value of the price10 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrice10() {
        return price10;
    }

    /**
     * Sets the value of the price10 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrice10(String value) {
        this.price10 = value;
    }

    /**
     * Gets the value of the amout10 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAmout10() {
        return amout10;
    }

    /**
     * Sets the value of the amout10 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAmout10(String value) {
        this.amout10 = value;
    }

    /**
     * Gets the value of the price11 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrice11() {
        return price11;
    }

    /**
     * Sets the value of the price11 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrice11(String value) {
        this.price11 = value;
    }

    /**
     * Gets the value of the amout11 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAmout11() {
        return amout11;
    }

    /**
     * Sets the value of the amout11 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAmout11(String value) {
        this.amout11 = value;
    }

    /**
     * Gets the value of the price12 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrice12() {
        return price12;
    }

    /**
     * Sets the value of the price12 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrice12(String value) {
        this.price12 = value;
    }

    /**
     * Gets the value of the amout12 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAmout12() {
        return amout12;
    }

    /**
     * Sets the value of the amout12 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAmout12(String value) {
        this.amout12 = value;
    }

    /**
     * Gets the value of the price13 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrice13() {
        return price13;
    }

    /**
     * Sets the value of the price13 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrice13(String value) {
        this.price13 = value;
    }

    /**
     * Gets the value of the amout13 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAmout13() {
        return amout13;
    }

    /**
     * Sets the value of the amout13 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAmout13(String value) {
        this.amout13 = value;
    }

    /**
     * Gets the value of the price14 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrice14() {
        return price14;
    }

    /**
     * Sets the value of the price14 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrice14(String value) {
        this.price14 = value;
    }

    /**
     * Gets the value of the amout14 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAmout14() {
        return amout14;
    }

    /**
     * Sets the value of the amout14 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAmout14(String value) {
        this.amout14 = value;
    }

    /**
     * Gets the value of the price15 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrice15() {
        return price15;
    }

    /**
     * Sets the value of the price15 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrice15(String value) {
        this.price15 = value;
    }

    /**
     * Gets the value of the amout15 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAmout15() {
        return amout15;
    }

    /**
     * Sets the value of the amout15 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAmout15(String value) {
        this.amout15 = value;
    }

    /**
     * Gets the value of the price16 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrice16() {
        return price16;
    }

    /**
     * Sets the value of the price16 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrice16(String value) {
        this.price16 = value;
    }

    /**
     * Gets the value of the amout16 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAmout16() {
        return amout16;
    }

    /**
     * Sets the value of the amout16 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAmout16(String value) {
        this.amout16 = value;
    }

    /**
     * Gets the value of the price17 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrice17() {
        return price17;
    }

    /**
     * Sets the value of the price17 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrice17(String value) {
        this.price17 = value;
    }

    /**
     * Gets the value of the amout17 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAmout17() {
        return amout17;
    }

    /**
     * Sets the value of the amout17 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAmout17(String value) {
        this.amout17 = value;
    }

    /**
     * Gets the value of the price18 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrice18() {
        return price18;
    }

    /**
     * Sets the value of the price18 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrice18(String value) {
        this.price18 = value;
    }

    /**
     * Gets the value of the amout18 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAmout18() {
        return amout18;
    }

    /**
     * Sets the value of the amout18 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAmout18(String value) {
        this.amout18 = value;
    }

    /**
     * Gets the value of the price19 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrice19() {
        return price19;
    }

    /**
     * Sets the value of the price19 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrice19(String value) {
        this.price19 = value;
    }

    /**
     * Gets the value of the amout19 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAmout19() {
        return amout19;
    }

    /**
     * Sets the value of the amout19 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAmout19(String value) {
        this.amout19 = value;
    }

    /**
     * Gets the value of the price20 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrice20() {
        return price20;
    }

    /**
     * Sets the value of the price20 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrice20(String value) {
        this.price20 = value;
    }

    /**
     * Gets the value of the amout20 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAmout20() {
        return amout20;
    }

    /**
     * Sets the value of the amout20 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAmout20(String value) {
        this.amout20 = value;
    }

    /**
     * Gets the value of the price21 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrice21() {
        return price21;
    }

    /**
     * Sets the value of the price21 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrice21(String value) {
        this.price21 = value;
    }

    /**
     * Gets the value of the amout21 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAmout21() {
        return amout21;
    }

    /**
     * Sets the value of the amout21 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAmout21(String value) {
        this.amout21 = value;
    }

    /**
     * Gets the value of the price22 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrice22() {
        return price22;
    }

    /**
     * Sets the value of the price22 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrice22(String value) {
        this.price22 = value;
    }

    /**
     * Gets the value of the amout22 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAmout22() {
        return amout22;
    }

    /**
     * Sets the value of the amout22 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAmout22(String value) {
        this.amout22 = value;
    }

    /**
     * Gets the value of the price23 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrice23() {
        return price23;
    }

    /**
     * Sets the value of the price23 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrice23(String value) {
        this.price23 = value;
    }

    /**
     * Gets the value of the amout23 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAmout23() {
        return amout23;
    }

    /**
     * Sets the value of the amout23 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAmout23(String value) {
        this.amout23 = value;
    }

    /**
     * Gets the value of the price24 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrice24() {
        return price24;
    }

    /**
     * Sets the value of the price24 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrice24(String value) {
        this.price24 = value;
    }

    /**
     * Gets the value of the amout24 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAmout24() {
        return amout24;
    }

    /**
     * Sets the value of the amout24 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAmout24(String value) {
        this.amout24 = value;
    }

    /**
     * Gets the value of the price25 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrice25() {
        return price25;
    }

    /**
     * Sets the value of the price25 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrice25(String value) {
        this.price25 = value;
    }

    /**
     * Gets the value of the amout25 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAmout25() {
        return amout25;
    }

    /**
     * Sets the value of the amout25 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAmout25(String value) {
        this.amout25 = value;
    }

    /**
     * Gets the value of the price26 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrice26() {
        return price26;
    }

    /**
     * Sets the value of the price26 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrice26(String value) {
        this.price26 = value;
    }

    /**
     * Gets the value of the amout26 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAmout26() {
        return amout26;
    }

    /**
     * Sets the value of the amout26 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAmout26(String value) {
        this.amout26 = value;
    }

    /**
     * Gets the value of the price27 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrice27() {
        return price27;
    }

    /**
     * Sets the value of the price27 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrice27(String value) {
        this.price27 = value;
    }

    /**
     * Gets the value of the amout27 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAmout27() {
        return amout27;
    }

    /**
     * Sets the value of the amout27 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAmout27(String value) {
        this.amout27 = value;
    }

    /**
     * Gets the value of the price28 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrice28() {
        return price28;
    }

    /**
     * Sets the value of the price28 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrice28(String value) {
        this.price28 = value;
    }

    /**
     * Gets the value of the amout28 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAmout28() {
        return amout28;
    }

    /**
     * Sets the value of the amout28 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAmout28(String value) {
        this.amout28 = value;
    }

    /**
     * Gets the value of the price29 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrice29() {
        return price29;
    }

    /**
     * Sets the value of the price29 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrice29(String value) {
        this.price29 = value;
    }

    /**
     * Gets the value of the amout29 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAmout29() {
        return amout29;
    }

    /**
     * Sets the value of the amout29 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAmout29(String value) {
        this.amout29 = value;
    }

    /**
     * Gets the value of the reason property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReason() {
        return reason;
    }

    /**
     * Sets the value of the reason property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReason(String value) {
        this.reason = value;
    }

}
