
package com.huawei.evcinterface.portalmgr;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for C2CTransferRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="C2CTransferRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SRCMSISDN" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DESTMSISDN" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DEONO1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="VALUE1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DEONO2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="VALUE2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DEONO3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="VALUE3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DEONO4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="VALUE4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PIN" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "C2CTransferRequest", propOrder = {
    "srcmsisdn",
    "destmsisdn",
    "deono1",
    "value1",
    "deono2",
    "value2",
    "deono3",
    "value3",
    "deono4",
    "value4",
    "pin"
})
public class C2CTransferRequest {

    @XmlElement(name = "SRCMSISDN", required = true)
    protected String srcmsisdn;
    @XmlElement(name = "DESTMSISDN", required = true)
    protected String destmsisdn;
    @XmlElement(name = "DEONO1")
    protected String deono1;
    @XmlElement(name = "VALUE1", required = true)
    protected String value1;
    @XmlElement(name = "DEONO2")
    protected String deono2;
    @XmlElement(name = "VALUE2")
    protected String value2;
    @XmlElement(name = "DEONO3")
    protected String deono3;
    @XmlElement(name = "VALUE3")
    protected String value3;
    @XmlElement(name = "DEONO4")
    protected String deono4;
    @XmlElement(name = "VALUE4")
    protected String value4;
    @XmlElement(name = "PIN", required = true)
    protected String pin;

    /**
     * Gets the value of the srcmsisdn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSRCMSISDN() {
        return srcmsisdn;
    }

    /**
     * Sets the value of the srcmsisdn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSRCMSISDN(String value) {
        this.srcmsisdn = value;
    }

    /**
     * Gets the value of the destmsisdn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDESTMSISDN() {
        return destmsisdn;
    }

    /**
     * Sets the value of the destmsisdn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDESTMSISDN(String value) {
        this.destmsisdn = value;
    }

    /**
     * Gets the value of the deono1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDEONO1() {
        return deono1;
    }

    /**
     * Sets the value of the deono1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDEONO1(String value) {
        this.deono1 = value;
    }

    /**
     * Gets the value of the value1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVALUE1() {
        return value1;
    }

    /**
     * Sets the value of the value1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVALUE1(String value) {
        this.value1 = value;
    }

    /**
     * Gets the value of the deono2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDEONO2() {
        return deono2;
    }

    /**
     * Sets the value of the deono2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDEONO2(String value) {
        this.deono2 = value;
    }

    /**
     * Gets the value of the value2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVALUE2() {
        return value2;
    }

    /**
     * Sets the value of the value2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVALUE2(String value) {
        this.value2 = value;
    }

    /**
     * Gets the value of the deono3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDEONO3() {
        return deono3;
    }

    /**
     * Sets the value of the deono3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDEONO3(String value) {
        this.deono3 = value;
    }

    /**
     * Gets the value of the value3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVALUE3() {
        return value3;
    }

    /**
     * Sets the value of the value3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVALUE3(String value) {
        this.value3 = value;
    }

    /**
     * Gets the value of the deono4 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDEONO4() {
        return deono4;
    }

    /**
     * Sets the value of the deono4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDEONO4(String value) {
        this.deono4 = value;
    }

    /**
     * Gets the value of the value4 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVALUE4() {
        return value4;
    }

    /**
     * Sets the value of the value4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVALUE4(String value) {
        this.value4 = value;
    }

    /**
     * Gets the value of the pin property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPIN() {
        return pin;
    }

    /**
     * Sets the value of the pin property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPIN(String value) {
        this.pin = value;
    }

}
