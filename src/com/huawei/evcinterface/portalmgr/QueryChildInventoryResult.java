
package com.huawei.evcinterface.portalmgr;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for QueryChildInventoryResult complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="QueryChildInventoryResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DLName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="HPNO" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Rank" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="MainHPNO" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ParentHPNO" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="STOCK0" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="STOCK1" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="STOCK2" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="STOCK3" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="STOCK4" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="STOCK5" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="STOCK6" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="STOCK7" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="STOCK8" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="STOCK9" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="STOCK10" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="STOCK11" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="STOCK12" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="STOCK13" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="STOCK14" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="STOCK15" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="STOCK16" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="STOCK17" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="STOCK18" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="STOCK19" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="STOCK20" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="STOCK21" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="STOCK22" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="STOCK23" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="STOCK24" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="STOCK25" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="STOCK26" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="STOCK27" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="STOCK28" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="STOCK29" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="RegionID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QueryChildInventoryResult", propOrder = {
    "dlName",
    "hpno",
    "status",
    "rank",
    "mainHPNO",
    "parentHPNO",
    "stock0",
    "stock1",
    "stock2",
    "stock3",
    "stock4",
    "stock5",
    "stock6",
    "stock7",
    "stock8",
    "stock9",
    "stock10",
    "stock11",
    "stock12",
    "stock13",
    "stock14",
    "stock15",
    "stock16",
    "stock17",
    "stock18",
    "stock19",
    "stock20",
    "stock21",
    "stock22",
    "stock23",
    "stock24",
    "stock25",
    "stock26",
    "stock27",
    "stock28",
    "stock29",
    "regionID"
})
public class QueryChildInventoryResult {

    @XmlElement(name = "DLName", required = true)
    protected String dlName;
    @XmlElement(name = "HPNO", required = true)
    protected String hpno;
    @XmlElement(name = "Status")
    protected int status;
    @XmlElement(name = "Rank")
    protected int rank;
    @XmlElement(name = "MainHPNO", required = true)
    protected String mainHPNO;
    @XmlElement(name = "ParentHPNO", required = true)
    protected String parentHPNO;
    @XmlElement(name = "STOCK0")
    protected long stock0;
    @XmlElement(name = "STOCK1")
    protected long stock1;
    @XmlElement(name = "STOCK2")
    protected long stock2;
    @XmlElement(name = "STOCK3")
    protected long stock3;
    @XmlElement(name = "STOCK4")
    protected long stock4;
    @XmlElement(name = "STOCK5")
    protected long stock5;
    @XmlElement(name = "STOCK6")
    protected long stock6;
    @XmlElement(name = "STOCK7")
    protected long stock7;
    @XmlElement(name = "STOCK8")
    protected long stock8;
    @XmlElement(name = "STOCK9")
    protected long stock9;
    @XmlElement(name = "STOCK10")
    protected long stock10;
    @XmlElement(name = "STOCK11")
    protected long stock11;
    @XmlElement(name = "STOCK12")
    protected long stock12;
    @XmlElement(name = "STOCK13")
    protected long stock13;
    @XmlElement(name = "STOCK14")
    protected long stock14;
    @XmlElement(name = "STOCK15")
    protected long stock15;
    @XmlElement(name = "STOCK16")
    protected long stock16;
    @XmlElement(name = "STOCK17")
    protected long stock17;
    @XmlElement(name = "STOCK18")
    protected long stock18;
    @XmlElement(name = "STOCK19")
    protected long stock19;
    @XmlElement(name = "STOCK20")
    protected long stock20;
    @XmlElement(name = "STOCK21")
    protected long stock21;
    @XmlElement(name = "STOCK22")
    protected long stock22;
    @XmlElement(name = "STOCK23")
    protected long stock23;
    @XmlElement(name = "STOCK24")
    protected long stock24;
    @XmlElement(name = "STOCK25")
    protected long stock25;
    @XmlElement(name = "STOCK26")
    protected long stock26;
    @XmlElement(name = "STOCK27")
    protected long stock27;
    @XmlElement(name = "STOCK28")
    protected long stock28;
    @XmlElement(name = "STOCK29")
    protected long stock29;
    @XmlElement(name = "RegionID", required = true)
    protected String regionID;

    /**
     * Gets the value of the dlName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDLName() {
        return dlName;
    }

    /**
     * Sets the value of the dlName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDLName(String value) {
        this.dlName = value;
    }

    /**
     * Gets the value of the hpno property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHPNO() {
        return hpno;
    }

    /**
     * Sets the value of the hpno property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHPNO(String value) {
        this.hpno = value;
    }

    /**
     * Gets the value of the status property.
     * 
     */
    public int getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     */
    public void setStatus(int value) {
        this.status = value;
    }

    /**
     * Gets the value of the rank property.
     * 
     */
    public int getRank() {
        return rank;
    }

    /**
     * Sets the value of the rank property.
     * 
     */
    public void setRank(int value) {
        this.rank = value;
    }

    /**
     * Gets the value of the mainHPNO property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMainHPNO() {
        return mainHPNO;
    }

    /**
     * Sets the value of the mainHPNO property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMainHPNO(String value) {
        this.mainHPNO = value;
    }

    /**
     * Gets the value of the parentHPNO property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParentHPNO() {
        return parentHPNO;
    }

    /**
     * Sets the value of the parentHPNO property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParentHPNO(String value) {
        this.parentHPNO = value;
    }

    /**
     * Gets the value of the stock0 property.
     * 
     */
    public long getSTOCK0() {
        return stock0;
    }

    /**
     * Sets the value of the stock0 property.
     * 
     */
    public void setSTOCK0(long value) {
        this.stock0 = value;
    }

    /**
     * Gets the value of the stock1 property.
     * 
     */
    public long getSTOCK1() {
        return stock1;
    }

    /**
     * Sets the value of the stock1 property.
     * 
     */
    public void setSTOCK1(long value) {
        this.stock1 = value;
    }

    /**
     * Gets the value of the stock2 property.
     * 
     */
    public long getSTOCK2() {
        return stock2;
    }

    /**
     * Sets the value of the stock2 property.
     * 
     */
    public void setSTOCK2(long value) {
        this.stock2 = value;
    }

    /**
     * Gets the value of the stock3 property.
     * 
     */
    public long getSTOCK3() {
        return stock3;
    }

    /**
     * Sets the value of the stock3 property.
     * 
     */
    public void setSTOCK3(long value) {
        this.stock3 = value;
    }

    /**
     * Gets the value of the stock4 property.
     * 
     */
    public long getSTOCK4() {
        return stock4;
    }

    /**
     * Sets the value of the stock4 property.
     * 
     */
    public void setSTOCK4(long value) {
        this.stock4 = value;
    }

    /**
     * Gets the value of the stock5 property.
     * 
     */
    public long getSTOCK5() {
        return stock5;
    }

    /**
     * Sets the value of the stock5 property.
     * 
     */
    public void setSTOCK5(long value) {
        this.stock5 = value;
    }

    /**
     * Gets the value of the stock6 property.
     * 
     */
    public long getSTOCK6() {
        return stock6;
    }

    /**
     * Sets the value of the stock6 property.
     * 
     */
    public void setSTOCK6(long value) {
        this.stock6 = value;
    }

    /**
     * Gets the value of the stock7 property.
     * 
     */
    public long getSTOCK7() {
        return stock7;
    }

    /**
     * Sets the value of the stock7 property.
     * 
     */
    public void setSTOCK7(long value) {
        this.stock7 = value;
    }

    /**
     * Gets the value of the stock8 property.
     * 
     */
    public long getSTOCK8() {
        return stock8;
    }

    /**
     * Sets the value of the stock8 property.
     * 
     */
    public void setSTOCK8(long value) {
        this.stock8 = value;
    }

    /**
     * Gets the value of the stock9 property.
     * 
     */
    public long getSTOCK9() {
        return stock9;
    }

    /**
     * Sets the value of the stock9 property.
     * 
     */
    public void setSTOCK9(long value) {
        this.stock9 = value;
    }

    /**
     * Gets the value of the stock10 property.
     * 
     */
    public long getSTOCK10() {
        return stock10;
    }

    /**
     * Sets the value of the stock10 property.
     * 
     */
    public void setSTOCK10(long value) {
        this.stock10 = value;
    }

    /**
     * Gets the value of the stock11 property.
     * 
     */
    public long getSTOCK11() {
        return stock11;
    }

    /**
     * Sets the value of the stock11 property.
     * 
     */
    public void setSTOCK11(long value) {
        this.stock11 = value;
    }

    /**
     * Gets the value of the stock12 property.
     * 
     */
    public long getSTOCK12() {
        return stock12;
    }

    /**
     * Sets the value of the stock12 property.
     * 
     */
    public void setSTOCK12(long value) {
        this.stock12 = value;
    }

    /**
     * Gets the value of the stock13 property.
     * 
     */
    public long getSTOCK13() {
        return stock13;
    }

    /**
     * Sets the value of the stock13 property.
     * 
     */
    public void setSTOCK13(long value) {
        this.stock13 = value;
    }

    /**
     * Gets the value of the stock14 property.
     * 
     */
    public long getSTOCK14() {
        return stock14;
    }

    /**
     * Sets the value of the stock14 property.
     * 
     */
    public void setSTOCK14(long value) {
        this.stock14 = value;
    }

    /**
     * Gets the value of the stock15 property.
     * 
     */
    public long getSTOCK15() {
        return stock15;
    }

    /**
     * Sets the value of the stock15 property.
     * 
     */
    public void setSTOCK15(long value) {
        this.stock15 = value;
    }

    /**
     * Gets the value of the stock16 property.
     * 
     */
    public long getSTOCK16() {
        return stock16;
    }

    /**
     * Sets the value of the stock16 property.
     * 
     */
    public void setSTOCK16(long value) {
        this.stock16 = value;
    }

    /**
     * Gets the value of the stock17 property.
     * 
     */
    public long getSTOCK17() {
        return stock17;
    }

    /**
     * Sets the value of the stock17 property.
     * 
     */
    public void setSTOCK17(long value) {
        this.stock17 = value;
    }

    /**
     * Gets the value of the stock18 property.
     * 
     */
    public long getSTOCK18() {
        return stock18;
    }

    /**
     * Sets the value of the stock18 property.
     * 
     */
    public void setSTOCK18(long value) {
        this.stock18 = value;
    }

    /**
     * Gets the value of the stock19 property.
     * 
     */
    public long getSTOCK19() {
        return stock19;
    }

    /**
     * Sets the value of the stock19 property.
     * 
     */
    public void setSTOCK19(long value) {
        this.stock19 = value;
    }

    /**
     * Gets the value of the stock20 property.
     * 
     */
    public long getSTOCK20() {
        return stock20;
    }

    /**
     * Sets the value of the stock20 property.
     * 
     */
    public void setSTOCK20(long value) {
        this.stock20 = value;
    }

    /**
     * Gets the value of the stock21 property.
     * 
     */
    public long getSTOCK21() {
        return stock21;
    }

    /**
     * Sets the value of the stock21 property.
     * 
     */
    public void setSTOCK21(long value) {
        this.stock21 = value;
    }

    /**
     * Gets the value of the stock22 property.
     * 
     */
    public long getSTOCK22() {
        return stock22;
    }

    /**
     * Sets the value of the stock22 property.
     * 
     */
    public void setSTOCK22(long value) {
        this.stock22 = value;
    }

    /**
     * Gets the value of the stock23 property.
     * 
     */
    public long getSTOCK23() {
        return stock23;
    }

    /**
     * Sets the value of the stock23 property.
     * 
     */
    public void setSTOCK23(long value) {
        this.stock23 = value;
    }

    /**
     * Gets the value of the stock24 property.
     * 
     */
    public long getSTOCK24() {
        return stock24;
    }

    /**
     * Sets the value of the stock24 property.
     * 
     */
    public void setSTOCK24(long value) {
        this.stock24 = value;
    }

    /**
     * Gets the value of the stock25 property.
     * 
     */
    public long getSTOCK25() {
        return stock25;
    }

    /**
     * Sets the value of the stock25 property.
     * 
     */
    public void setSTOCK25(long value) {
        this.stock25 = value;
    }

    /**
     * Gets the value of the stock26 property.
     * 
     */
    public long getSTOCK26() {
        return stock26;
    }

    /**
     * Sets the value of the stock26 property.
     * 
     */
    public void setSTOCK26(long value) {
        this.stock26 = value;
    }

    /**
     * Gets the value of the stock27 property.
     * 
     */
    public long getSTOCK27() {
        return stock27;
    }

    /**
     * Sets the value of the stock27 property.
     * 
     */
    public void setSTOCK27(long value) {
        this.stock27 = value;
    }

    /**
     * Gets the value of the stock28 property.
     * 
     */
    public long getSTOCK28() {
        return stock28;
    }

    /**
     * Sets the value of the stock28 property.
     * 
     */
    public void setSTOCK28(long value) {
        this.stock28 = value;
    }

    /**
     * Gets the value of the stock29 property.
     * 
     */
    public long getSTOCK29() {
        return stock29;
    }

    /**
     * Sets the value of the stock29 property.
     * 
     */
    public void setSTOCK29(long value) {
        this.stock29 = value;
    }

    /**
     * Gets the value of the regionID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegionID() {
        return regionID;
    }

    /**
     * Sets the value of the regionID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegionID(String value) {
        this.regionID = value;
    }

}
